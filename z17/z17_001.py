f = open('17001.txt','r')
b = int(f.readline())
count = 0
summ = 0

while True:
    a = b
    s = f.readline()
    if not s:
        break
    b = int(s)
    if (a % 3 == 0) or (b % 3 == 0):
        count += 1
        if (a + b > summ):
            summ = a + b
f.close()

print('Кол-во пар: ', count, ', макс. сумма: ', summ, '.')   
    