f = [int(x) for x in open('z17_ 37348.txt', 'r')]
count = 0
summ = 0

for i in range(len(f) - 1):
    for j in range(i + 1, len(f)):
        c = f[i] * f[j]
        if c % 34 != 0:
            count += 1
            if summ < f[i] + f[j]:
                summ = f[i] + f[j]

print(count, summ)