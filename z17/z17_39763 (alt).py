def fu(a, b, c):
    t = sorted([a, b, c])
    if (t[2]**2 < (t[0]**2 + t[1]**2)):
        return True
    else:
        return False

count = 0
summ = 0

f = open('z17_39763.txt', 'r').readlines()
f = [int(s) for s in f]
for i in range(len(f) - 2):
    if fu(f[i], f[i + 1], f[i + 2]):
        count += 1
        summ = max(summ, sum(f[i: i + 3]))
        
print('Кол-во найденных троек: ', count, ', макс. сумма: ', summ)