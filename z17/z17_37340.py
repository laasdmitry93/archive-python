count = 0
summ = 0

a = [int(s) for s in open('z17_37340.txt')]

for i in range(len(a) - 1):
    for j in range(i + 1, len(a)):
        if ((a[i] - a[j]) % 2 == 0) and ((a[i] % 31 == 0) or (a[j] % 31 == 0)):
            count += 1
            if (a[i] + a[j] > summ):
                summ = a[i] + a[j]

print('Кол-во пар: ', count, ', макс. сумма: ', summ)