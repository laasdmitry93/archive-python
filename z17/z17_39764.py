def triangle(a, b, c):
    p_list = sorted([a, b, c])
    if (p_list[2] ** 2 < p_list[0] ** 2 + p_list[1] ** 2):
        return a + b + c
    else:
        return 0

f = [int(x) for x in open('z17_39766.txt', 'r')]

count = 0
summ = 0

for i in range(len(f) - 2):
    p = triangle(f[i], f[i + 1], f[i + 2])

    if p > 0:
        count += 1
        summ = max(summ, p)

print(count, summ)