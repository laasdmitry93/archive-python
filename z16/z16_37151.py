def F(n):
    if (n <= 1):
        return 0
    elif (n > 1) and (n % 2 == 1):
        return F(n - 1) + 3 * n * n
    return n // 2 + F(n - 1) + 2
print(F(49))