def f(x):
    if x == 0:
        return 0
    elif x % 2 == 0:
        return f(x // 2)
    
    return 1 + f((x - 1) // 2)

count = 0

for i in range(1001):
    if f(i) == 3:
        count +=1
        
print(count)