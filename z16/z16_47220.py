def F(n):
    r = 1
    while n > 0:
        r *= n
        n -= 1
    return r

print(F(2023) // F(2020))