import turtle as t

t.speed(10)
cfRes = 30

for N in range(4):
    t.forward(12 * cfRes)
    t.right(90)
    
for K in range(3):
    t.forward(12 * cfRes)
    t.right(120)    

t.up()

for x in range(13):
    for y in range(13):
        t.goto(x * cfRes, -y * cfRes)
        t.dot(4)

t.done()