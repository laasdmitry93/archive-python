import turtle as tr

tr.speed(10)
cfRes = 25
tr.left(90)

for N in range(4):
    tr.forward(5 * cfRes)
    tr.right(90)
    tr.forward(10 * cfRes)
    tr.right(90)

tr.up()

for x in range(11):
    for y in range(6):
        tr.goto(x * cfRes, y * cfRes)
        tr.dot(4)

tr.done()