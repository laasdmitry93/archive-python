import turtle as t

m = 40
t.lt(90)

for i in range(3):
    t.forward(10*m)
    t.right(120)
t.up()

for x in range(11):
    for y in range(11):
        t.goto(x*m, y*m)
        t.dot(4)
        
t.done() # отображение окна с результатом работы программы