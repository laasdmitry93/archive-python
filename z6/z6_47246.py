import turtle as tr

tr.speed(10) # скорость черепахи
cfRes = 20 # коэф. масштаб.
tr.left(90) # поворот влево на 90

# Начало основной программы

for N in range(4):
    tr.forward(14 * cfRes) # домножаю на cfRes для увеличения размера (масштаба) фигуры
    tr.right(120) # поворот вправо на 120

tr.up() # поднимаю перо вверх без черчения линий

for x in range(20):
    for y in range(20):
        tr.goto(x * cfRes, y * cfRes) # задаю размер сетки
        tr.dot(4) # в пересечении клеток ставит точку размером 4
        
# Конец основной программы

tr.done() # Выводим окно с результатом

# Ответ: 78