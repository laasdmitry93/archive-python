import turtle as tr

tr.speed(10)
cfRes = 20
tr.left(90)

for N in range(4):
    tr.forward(12 * cfRes)
    tr.right(90)

tr.right(30)

for T in range(3):
    tr.forward(8 * cfRes)
    tr.right(60)
    tr.forward(8 * cfRes)
    tr.right(120)
    
tr.up()
    
for x in range(13):
    for y in range(13):
        tr.goto(x * cfRes, y * cfRes)
        tr.dot(4)
    
tr.done()