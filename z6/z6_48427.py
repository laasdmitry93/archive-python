import turtle as trt

trt.speed(10)
cfRes = 5

for N in range(11):
    trt.forward(36 * cfRes)
    trt.right(72)

trt.up()

print('x, y: ', trt.pos())

trt.done()