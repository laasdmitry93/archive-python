def tst(t):
    shift = ord('A')
    rslt = ''
    cnt = -1
    for i in range(26):
        tmp = t.count(chr(i + shift))
        if tmp >= cnt:
            rslt = chr(i + shift)
            cnt = tmp
    return rslt

f = open('z24_35482.txt', 'r').readlines()

res = 'G'
count = 2 ** 23
for i in range(len(f)):
    s = f[i]
    if s.count('G') < count:
        count = s.count('G')
        res = tst(s)

print(res)