f = open('24_demo.txt', 'r').readline()

count = 0
rez = 1

for i in range(len(f) - 1):
    if (f[i] != f[i + 1]):
        count += 1
    else:
        if (rez < count):
            rez = count
        count = 1
        
print(rez)
