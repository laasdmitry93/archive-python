def func(t):
    shift = ord('A')
    rslt = -1
    for u in range(shift, shift + 26):
        d=t.rfind(chr(u)) - t.find(chr(u))
        if d > rslt:
            rslt = d
    return rslt

f = open('z24_35998.txt', 'r').readlines()

search = 'A'
count = 2 ** 20
result = -1

for s in f:
    if s.count(search) < 25:
        tmp = func(s)
        if tmp > result:
            result = tmp
            
print(result)        