def func(t): # ф-ция примет на вход строку t
    shift = ord('A') # это сдвиг для получения буквы из счётчика цикла
    rslt = ''
    cnt = -1
    for u in range(shift, shift + 26):
        tmp = t.count(chr(u))
        if tmp >= cnt:
            rslt = chr(u)
            cnt = tmp
    return rslt

# Открываем и считываем файл в память
f = open('z24_35913.txt', 'r').readlines()
# То, что ищем
search = 'N'
# Объём файла 1МБ
count = 2 ** 20
# Перебор ...
for s in f:
    # Присваиваем s значение f[i]
    #s = f[i]
    # Проверям кол-во истин. букв с объёмом файла
    if s.count(search) < count:
        # Передаём значение ...
        count = s.count(search)
        # Присвиваем значение ф-ии в переменную result
        result = func(s)
# Выводим результат
print(result)