f = open('z24_33769.txt', 'r').readline()
bu=[0]*26
for i in range(len(f)-3):
    if f[i]==f[i+1]:
        bu[ord(f[i+2])-ord('A')]+=1
msym=max(bu)
for i in range(26):
    if bu[i]==msym:
        msym=i
        break
print(chr(msym + ord('A')))