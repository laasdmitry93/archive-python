def tst(t):
    shift = ord('A')
    lst = [0] * 26
    indx = -1
    cnt = -1
    for i in range(26):
        lst[i] = t.count(chr(i + shift))
        if lst[i] >= cnt:
            indx = i
            cnt = lst[i]
    return chr(shift + indx)

f = open('z24_35482.txt', 'r').readlines()

res = 'G'
count = 2 ** 23
for i in range(len(f)):
    s = f[i]
    if s.count('G') < count:
        count = s.count('G')
        res = tst(s)

print(res)