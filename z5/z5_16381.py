for n in range(5, 10000):
    if n % 2 == 1:
        binN = bin(n)[2:-1] + '10'
    else:
        binN = bin(n)[2:-1] + '01'
    answ = int(binN, 2)
    if answ == 2018:
        print(n)
        break