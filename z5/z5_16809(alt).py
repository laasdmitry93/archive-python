for n in range(256):
    binN = bin(n)[2:]
    altN = binN
    answ = ''
    while len(altN) < 8:
        altN = '0' + altN
    for i in altN:
        if i == '0':
            answ += '1'
        else:
            answ += '0'
    delta = int(answ, 2) - int(binN, 2)
    if delta == 133:
        print(n)