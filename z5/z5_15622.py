for n in range(1, 1000):
    binN = bin(n)[2:]
    sumbN = binN.count('1')
    if sumbN % 2 == 1:
        newN = binN + '11'
    else:
        newN = binN + '10'
    r = int(newN, 2)
    if r > 114:
        print(r)
        break