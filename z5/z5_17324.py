start = 10
end = 1000
r = set()

for n in range(start, end + 1):
    binN = bin(n)[2:]
    newBinN = ''
    if binN.index('1') == 0:
        newBinN = binN[1:]
    if newBinN.count('1') != 0:
        newBinN = newBinN[newBinN.index('1'):]
    decN = int(newBinN, 2)
    delta = n - decN
    r.add(delta)

print(len(r))