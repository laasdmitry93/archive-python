for n in range(256):
    binN = ('00000000' + bin(n)[2:])[-8:]
    altN = ''
    for i in binN:
        if i == '0':
            altN += '1'
        else:
            altN += '0'
    delta = int(altN, 2) - int(binN, 2)
    if delta == 133:
        print(n)