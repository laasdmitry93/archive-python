def func(x):
    if x == 2:
        return False
    d = 3
    fn = True
    while d ** 2 < x:
        if x % d == 0:
            fn = False
            break
        d += 2
    return fn

def funcStart(q):
    strt = q + 1 + q % 2
    return strt

start = 245690
end = 245756
position = 0

for i in range(funcStart(start), end + 1, 2):
    position += 2
    if func(i):
        print(position, i)