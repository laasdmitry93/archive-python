def func(g):
    lst = []
    d = 1
    count = 0
    while d ** 2 <= g:
        if g % d == 0:
            count += 1
            if d % 2 == 0:
                lst.append(d)
            if (g // d) % 2 == 0:
                lst.append(g // d)
        if count > 4:
            break
        d += 1
    return lst

start = 110203
end = 110245

for i in range(start, end + 1):
    if len(func(i)) == 4:
        print(sorted(func(i)))