def checkNum(x):
    if x % 2 == 0:
        if x == 2:
            return True
        else:
            return False
    else:
        d = 3
        fn = True
        while d ** 2 <= x:
            if x % d == 0:
                fn = False
                break
        d += 2
    return fn

def quattro(x):
    j = int(x ** (1 / 4))
    if j ** 4 == x and checkNum(j):
        return x // j
    else:
        return 0

start = 289123456
end = 389123456

for i in range(start, end + 1):
    q = quattro(i)
    if q != 0:
        print(i, q)