def checkNum(x):
    lst = []
    d = 1
    while d ** 2 < x:
        if x % d == 0:
            j = (x // d) - d
            if j <= 100:
                lst.append(j)
        d += 1
    return lst

start = 1000000
end = 2000000

for i in range(start, end + 1):
    if len(checkNum(i)) > 2:
        print(i, ' ', sorted(checkNum(i)))