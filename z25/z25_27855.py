def func(x):
    lst = []
    d = 1
    two = 2
    count = 0
    while d ** two <= x:
        if x % d == 0:
            count += 1
            if d % count == 0:
                lst.append(d)
            if (x // d) % two == 0:
                lst.append(x // d)
        if count > 6:
            break
        d += 1
    return lst

start = 95632
end = 95700

for i in range(start, end + 1):
    if len(func(i)) == 6:
        print(sorted(func(i)))