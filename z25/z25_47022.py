def mainFunc(x):
    lst = []
    d = 2
    while d ** 2 < x:
        if x % d == 0:
            lst.append(d)
            lst.append(x // d)
        d += 1
    if d ** 2 == x:
        lst.append(d)
    if len(lst) < 5:
        return 0
    else:
        return sorted(lst)[-5]

start = 300000001
count = 0

while count < 5:
    h = mainFunc(start)
    if h != 0:
        count += 1
        print(h)
    start += 1