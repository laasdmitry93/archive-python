start = 174457
end = 174505

for i in range(start, end + 1):
    d = 2
    count = 0
    while d ** 2 < i:
        if i % d == 0:
            count += 1
            dd = d
        if count > 1:
            break
        d += 1
    if count == 1 and d ** 2 > 1:
        print(dd, i // dd)