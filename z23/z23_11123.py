k = 23
lst = [0] * k
lst[22] = 1

for i in range(k - 1, 1, -1):
    if (i - 2 > 1):
        lst[i - 2] += lst[i]
    if (i - 5 > 1):
        lst[i - 5] += lst[i]

print(lst[2])