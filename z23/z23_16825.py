start = 3
answ = 16
end = 17
lst = [0] * end
lst[start] = 1

for N in range(start, end):
    if (N + 1 == 6) or (N + 1 == 12):
        lst[N + 1] == 0
    elif (N + 1 < end):
        lst[N + 1] += lst[N]
    if (N * 2 == 6) or (N * 2 == 12):
        lst[N * 2] == 0
    elif (N * 2 < end):
        lst[N * 2] += lst[N]
    if (N + 3 == 6) or (N + 3 == 12):
        lst[N + 3] == 0
    elif (N + 3 < end):
        lst[N + 3] += lst[N]

print(lst[answ])