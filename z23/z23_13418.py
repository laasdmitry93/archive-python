k = 28
lst = [0] * k
lst[1] = 1

for N in range(1, k):
    if (N + 1 == 26):
        lst[N + 1] == 0
    elif (N + 1 < k):
        lst[N + 1] += lst[N]
    if (2 * N + 1 < k):
        lst[2 * N + 1] += lst[N]

print(lst[27])