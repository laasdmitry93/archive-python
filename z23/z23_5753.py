k = 16
lst = [0] * k
lst[2] = 1

for i in range(2, k):
    if (i + 1 < k):
        lst[i + 1] += lst[i]
    if (i + 5 < k):
        lst[i + 5] += lst[i]

print(lst[15])