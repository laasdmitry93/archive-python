k = 17
lst = [0] * k
lst[2] = 1

for i in range(2, k):
    if i + 1 < k:
        lst[i + 1] += lst[i]
    if i * 2 < k:
        lst[i * 2] += lst[i]
    if 2 * i + 1 < k:
        lst[2 * i + 1] += lst[i]

print(lst[16])