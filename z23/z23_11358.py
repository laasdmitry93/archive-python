start = 3
answ = 12
end = 13
lst = [0] * end
lst[3] = 1

for N in range(3, 10):
    if (N + 1 < 11):
        lst[N + 1] += lst[N]
    if (N + 2 < 11):
        lst[N + 2] += lst[N]
    if (N * 2 < 11):
        lst[N * 2] += lst[N]
        
for N in range(10, end):
    if (N + 1 < end):
        lst[N + 1] += lst[N]
    if (N + 2 < end):
        lst[N + 2] += lst[N]
    if (N * 2 < end):
        lst[N * 2] += lst[N]
        
print(lst[answ])