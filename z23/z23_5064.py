k = 14
lst = [0] * k
lst[1] = 1

for i in range(1, k):
    if (i + 1 < k):
        lst[i + 1] += lst[i]
    if (i * 2 < k):
        lst[i * 2] += lst[i]
    if (i * 3 < k):
        lst[i * 3] += lst[i]

print(lst[13])