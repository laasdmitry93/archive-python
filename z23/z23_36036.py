k = 24
lst = [0] * k
lst[23] = 1

for N in range(k - 1, 1, -1):
    if (N - 2 > 1):
        lst[N - 2] += lst[N]
    if (N - 5 > 1):
        lst[N - 5] += lst[N]
    
print(lst[2])