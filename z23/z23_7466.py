k = 31
lst = [0] * k
lst[21] = 1

for i in range(1, k):
    if (i + 1 < k):
        lst[i + 1] += lst[i]
    if (i + 2 < k):
        lst[i + 2] += lst[i]
    if (i + 4 < k):
        lst[i + 4] += lst[i]

print(lst[30])