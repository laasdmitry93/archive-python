k = 58
lst = [0] * k
lst[35] = 1

for i in range(35, k):
    if (i + 1 < k):
        lst[i + 1] += lst[i]
    if (i + 10 < k):
        lst[i + 10] += lst[i]

print(lst[57])