def giperbola(A):
    for x in range(9):
        for y in range(19):
            if (y <= 2 * x) and (x * y >= A):
                return False
    return True

t = 1

while not giperbola(t):
    t += 1

print(t)