def F(x, y):
    if x > y or x == 11:
        return 0
    elif x == y:
        return 1
    return F(x + 1, y) + F(x * 2, y) + F(x * 3, y)
print(F(2, 15) * F(15, 25))