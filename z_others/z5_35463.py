start = 1
end = 1000
for N in range(start, end):
    binN = bin(N)[2:]
    t = binN
    for i in range(3):
        cbNOne = t.count('1')
        cbNZero = t.count('0')
        if cbNOne == cbNZero:
           t += binN[-1]
        else:
            if cbNOne > cbNZero:
                t += '0'
            else:
                t += '1'
    r = int(t, 2)
    if N > 99 and r % 4 == 0:
        print(N)
        break