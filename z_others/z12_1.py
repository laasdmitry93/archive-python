for n in range(1, 200):
    strk = '3' + '5' * n
    while '25' in strk or '355' in strk or '555' in strk:
        if '25' in strk:
            strk = strk.replace('25', '3', 1)
        if '355' in strk:
            strk = strk.replace('355', '52', 1)
        if '555' in strk:
            strk = strk.replace('555', '23', 1)
    summ = sum(map(int, strk))    
    if summ == 27:
        print(n)
        break