f = [int(x) for x in open('17_7267.txt', 'r')]
y = min(f)
count = 0
summ = 0

for i in range(len(f) - 1):
    n = f[i]
    t = f[i + 1]
    if n % 117 == y or t % 117 == y:
        summ = max(n + t, summ)
        count += 1
print(count, summ)