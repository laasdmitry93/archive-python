m = []
for n in range(4, 100):
    strk = '3' + '5' * n
    while '25' in strk or '355' in strk or '555' in strk:
        if '25' in strk:
            strk = strk.replace('25', '32', 1)
        if '355' in strk:
            strk = strk.replace('355', '25', 1)
        if '555' in strk:
            strk = strk.replace('555', '3', 1)
    summ = sum(map(int, strk))
    if summ == 17:
        m.append(n)
print(min(m))