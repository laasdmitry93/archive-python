strk = '1' + '0' * 80

while '10' in strk or '1' in strk:
    if '10' in strk:
        strk = strk.replace('10', '001', 1)
    else:
        strk = strk.replace('1', '000', 1)
print(len(strk))