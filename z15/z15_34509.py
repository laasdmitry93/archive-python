def funcA(A, k):
    r = True
    for x in range(k):
        if not (x & 28 == 0 and x & 45 == 0 or x & 17 != 0 or x & A != 0):
            r = False
            break
    return r

n = 64

for i in range(n):
    if funcA(i, n):
        print(i)
        break