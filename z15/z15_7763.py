def funcM(A):
    r = True
    if not ((5 <= A <= 30) == (14 <= A <= 23)):
        r = False
    return r

for x in range(35):
    if not funcM(x):
        print(x)